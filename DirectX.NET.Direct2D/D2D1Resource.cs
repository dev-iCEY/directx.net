﻿#region Includes

using System;
using System.Runtime.InteropServices;

using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    public class D2D1Resource : Unknown, ID2D1Resource {
        protected new const uint LastMethodId = Unknown.LastMethodId + 1u;
        protected new readonly int MethodsCount = typeof(ID2D1Resource).GetMethods().Length;

        public D2D1Resource(IntPtr objectPtr) : base(objectPtr) {
            AddMethodsToVTableList(base.MethodsCount, MethodsCount);
            MethodsCount = base.MethodsCount + MethodsCount;
        }

        public void GetFactory(out ID2D1Factory factory) {
            GetMethodDelegate<GetFactoryDelegate>().Invoke(this, out IntPtr factoryPtr);
            factory = new D2D1Factory(factoryPtr);
        }

        [ComMethodId(Unknown.LastMethodId + 1u),
         UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void GetFactoryDelegate(IntPtr thisPtr, out IntPtr factoryPtr);
    }
}