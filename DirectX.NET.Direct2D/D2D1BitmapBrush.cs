﻿#region Usings

using System;
using System.Runtime.InteropServices;
using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D
{
    /// <summary>
    ///     A bitmap brush allows a bitmap to be used to fill a geometry.
    /// </summary>
    public class D2D1BitmapBrush : D2D1Brush, ID2D1BitmapBrush
    {
        protected new const uint LastMethodId = D2D1Brush.LastMethodId + 8u;
        protected new readonly int MethodsCount = typeof(ID2D1BitmapBrush).GetMethods().Length;


        public D2D1BitmapBrush(IntPtr objectPtr) : base(objectPtr)
        {
            AddMethodsToVTableList(base.MethodsCount, MethodsCount);
            MethodsCount = base.MethodsCount + MethodsCount;
        }

        /// <summary>
        ///     Sets how the bitmap is to be treated outside of its natural extent on the X axis.
        /// </summary>
        public void SetExtendModeX(D2D1ExtendMode extendModeX)
        {
            GetMethodDelegate<SetExtendModeXDelegate>().Invoke(this, extendModeX);
        }

        /// <summary>
        ///     Sets how the bitmap is to be treated outside of its natural extent on the Y axis.
        /// </summary>
        public void SetExtendModeY(D2D1ExtendMode extendModeY)
        {
            GetMethodDelegate<SetExtendModeYDelegate>().Invoke(this, extendModeY);
        }

        /// <summary>
        ///     Sets the interpolation mode used when this brush is used.
        /// </summary>
        public void SetInterpolationMode(D2D1BitmapInterpolationMode interpolationMode)
        {
            GetMethodDelegate<SetInterpolationModeDelegate>().Invoke(this, interpolationMode);
        }

        /// <summary>
        ///     Sets the bitmap associated as the source of this brush.
        /// </summary>
        public void SetBitmap(ID2D1Bitmap bitmap)
        {
            GetMethodDelegate<SetBitmapDelegate>().Invoke(this, (D2D1Bitmap) bitmap);
        }

        /// <summary>
        ///     Get how the bitmap is to be treated outside of its natural extent on the X axis.
        /// </summary>
        public D2D1ExtendMode GetExtendModeX()
        {
            return GetMethodDelegate<GetExtendModeXDelegate>().Invoke(this);
        }

        /// <summary>
        ///     Get how the bitmap is to be treated outside of its natural extent on the Y axis.
        /// </summary>
        public D2D1ExtendMode GetExtendModeY()
        {
            return GetMethodDelegate<GetExtendModeYDelegate>().Invoke(this);
        }

        /// <summary>
        ///     Get the interpolation mode used when this brush is used.
        /// </summary>
        public D2D1BitmapInterpolationMode GetInterpolationMode()
        {
            return GetMethodDelegate<GetInterpolationModeDelegate>().Invoke(this);
        }

        /// <summary>
        ///     Get the bitmap associated as the source of this brush.
        /// </summary>
        public void GetBitmap(out ID2D1Bitmap bitmap)
        {
            GetMethodDelegate<GetBitmapDelegate>().Invoke(this, out IntPtr bitmapPtr);
            bitmap = new D2D1Bitmap(bitmapPtr);
        }

        [ComMethodId(D2D1Brush.LastMethodId + 1u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SetExtendModeXDelegate(IntPtr thisPtr, D2D1ExtendMode extendModeX);

        [ComMethodId(D2D1Brush.LastMethodId + 2u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SetExtendModeYDelegate(IntPtr thisPtr, D2D1ExtendMode extendModeY);

        [ComMethodId(D2D1Brush.LastMethodId + 3u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SetInterpolationModeDelegate(IntPtr thisPtr,
            D2D1BitmapInterpolationMode bitmapInterpolationMode);

        [ComMethodId(D2D1Brush.LastMethodId + 4u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SetBitmapDelegate(IntPtr thisPtr, IntPtr bitmapPtr);

        [ComMethodId(D2D1Brush.LastMethodId + 5u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate D2D1ExtendMode GetExtendModeXDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Brush.LastMethodId + 6u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate D2D1ExtendMode GetExtendModeYDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Brush.LastMethodId + 7u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate D2D1BitmapInterpolationMode GetInterpolationModeDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Brush.LastMethodId + 8u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void GetBitmapDelegate(IntPtr thisPtr, out IntPtr bitmapPtr);
    }
}