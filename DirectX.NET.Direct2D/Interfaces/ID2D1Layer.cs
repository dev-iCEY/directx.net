﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D.Interfaces {
    /// <summary>
    ///     Represents the backing store required to render a layer.
    /// </summary>
    [Guid("2cd9069b-12e2-11dc-9fed-001143a055f9")]
    public interface ID2D1Layer : ID2D1Resource {
        D2D1SizeF GetSize();
    }
}