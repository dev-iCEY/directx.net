﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D.Interfaces {
    /// <summary>
    ///     Paints an area with a linear gradient.
    /// </summary>
    [Guid("2cd906ab-12e2-11dc-9fed-001143a055f9")]
    public interface ID2D1LinearGradientBrush : ID2D1Brush {
        /// <summary>
        ///     Sets the start point of the gradient in local coordinate space. This is not influenced by the geometry being
        ///     filled.
        /// </summary>
        void SetStartPoint(D2D1Point2F startPoint);

        /// <summary>
        ///     Sets the end point of the gradient in local coordinate space. This is not influenced by the geometry being filled.
        /// </summary>
        void SetEndPoint(D2D1Point2F endPoint);

        D2D1Point2F GetStartPoint();

        D2D1Point2F GetEndPoint();
    }
}