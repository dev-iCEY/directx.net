﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D.Interfaces {
    /// <summary>
    ///     A bitmap brush allows a bitmap to be used to fill a geometry.
    /// </summary>
    [Guid("2cd906aa-12e2-11dc-9fed-001143a055f9")]
    public interface ID2D1BitmapBrush : ID2D1Brush {
        /// <summary>
        ///     Sets how the bitmap is to be treated outside of its natural extent on the X axis.
        /// </summary>
        void SetExtendModeX(D2D1ExtendMode extendModeX);

        /// <summary>
        ///     Sets how the bitmap is to be treated outside of its natural extent on the Y axis.
        /// </summary>
        void SetExtendModeY(D2D1ExtendMode extendModeY);

        /// <summary>
        ///     Sets the interpolation mode used when this brush is used.
        /// </summary>
        void SetInterpolationMode(D2D1BitmapInterpolationMode interpolationMode);

        /// <summary>
        ///     Sets the bitmap associated as the source of this brush.
        /// </summary>
        void SetBitmap(ID2D1Bitmap bitmap);


        /// <summary>
        ///     Get how the bitmap is to be treated outside of its natural extent on the X axis.
        /// </summary>
        D2D1ExtendMode GetExtendModeX();

        /// <summary>
        ///     Get how the bitmap is to be treated outside of its natural extent on the Y axis.
        /// </summary>
        D2D1ExtendMode GetExtendModeY();

        /// <summary>
        ///     Get the interpolation mode used when this brush is used.
        /// </summary>
        D2D1BitmapInterpolationMode GetInterpolationMode();

        /// <summary>
        ///     Get the bitmap associated as the source of this brush.
        /// </summary>
        void GetBitmap(out ID2D1Bitmap bitmap);
    }
}