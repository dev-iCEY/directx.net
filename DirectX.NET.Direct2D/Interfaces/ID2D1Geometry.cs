﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D.Interfaces {
    /// <summary>
    ///     Represents a geometry resource and defines a set of helper methods for
    ///     manipulating and measuring geometric shapes. Interfaces that inherit from <see cref="ID2D1Geometry" /> define
    ///     specific
    ///     shapes.
    /// </summary>
    [Guid("2cd906a1-12e2-11dc-9fed-001143a055f9")]
    public interface ID2D1Geometry : ID2D1Resource {
        /// <summary>
        ///     Retrieve the bounds of the geometry, with an optional applied transform.
        /// </summary>
        int GetBounds(in Matrix3X2 worldTransform, out D2D1RectF bounds);

        int GetWidenedBounds();
    }
}