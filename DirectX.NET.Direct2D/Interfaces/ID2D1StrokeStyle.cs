﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D.Interfaces {
    /// <summary>
    ///     Resource interface that holds pen style properties.
    /// </summary>
    [Guid("2cd9069d-12e2-11dc-9fed-001143a055f9")]
    public interface ID2D1StrokeStyle : ID2D1Resource {
        D2D1CapStyle GetStartCap();
        D2D1CapStyle GetEndCap();
        D2D1CapStyle GetDashCap();
        float GetMiterLimit();
        D2D1LineJoin GetLineJoin();
        float GetDashOffset();
        D2D1DashStyle GetDashStyle();
        uint GetDashesCount();

        /// <summary>
        ///     Returns the dashes from the object into a user allocated array. The user must call GetDashesCount to retrieve the
        ///     required size.
        /// </summary>
        void GetDashes([In, Out] float[] dashed, uint countDashes);
    }
}