﻿#region Includes

using System;
using System.Runtime.InteropServices;

using DirectX.NET.Direct2D.Interfaces;
using DirectX.NET.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    public class D2D1Bitmap : D2D1Image, ID2D1Bitmap {
        protected new const uint LastMethodId = D2D1Resource.LastMethodId + 7u;
        protected new int MethodsCount = typeof(ID2D1Bitmap).GetMethods().Length;

        public D2D1Bitmap(IntPtr objectPtr) : base(objectPtr) {
            AddMethodsToVTableList(base.MethodsCount, MethodsCount);
            MethodsCount = base.MethodsCount + MethodsCount;
        }

        public D2D1SizeF GetSize() {
            return GetMethodDelegate<GetSizeDelegate>().Invoke(this);
        }

        public D2D1SizeU GetPixelSize() {
            return GetMethodDelegate<GetPixelSizeDelegate>().Invoke(this);
        }

        public PixelFormat GetPixelFormat() {
            return GetMethodDelegate<GetPixelFormatDelegate>().Invoke(this);
        }

        public void GetDpi(out float dpiX, out float dpiY) {
            GetMethodDelegate<GetDpiDelegate>().Invoke(this, out dpiX, out dpiY);
        }

        public int CopyFromBitmap(in D2D1Point2U destPoint, ID2D1Bitmap bitmap, in D2D1RectF srcRect) {
            return GetMethodDelegate<CopyFromBitmapDelegate>()
                .Invoke(this, in destPoint, (D2D1Bitmap) bitmap, in srcRect);
        }

        public int CopyFromRenderTarget(in D2D1Point2U destPoint, IUnknown renderTarget, in D2D1RectU srcRect) {
            return GetMethodDelegate<CopyFromRenderTargetDelegate>()
                .Invoke(this, in destPoint, (Unknown) renderTarget, in srcRect);
        }

        public int CopyFromMemory(in D2D1RectU dstRect, IntPtr srcData, uint pitch) {
            return GetMethodDelegate<CopyFromMemoryDelegate>().Invoke(this, in dstRect, srcData, pitch);
        }

        [ComMethodId(D2D1Resource.LastMethodId + 1u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate D2D1SizeF GetSizeDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Resource.LastMethodId + 2u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate D2D1SizeU GetPixelSizeDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Resource.LastMethodId + 3u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate PixelFormat GetPixelFormatDelegate(IntPtr thisPtr);

        [ComMethodId(D2D1Resource.LastMethodId + 4u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void GetDpiDelegate(IntPtr thisPtr, out float dpiX, out float dpiY);

        [ComMethodId(D2D1Resource.LastMethodId + 5u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate int CopyFromBitmapDelegate(IntPtr thisPtr, in D2D1Point2U destPoint, IntPtr bitmapPtr,
            in D2D1RectF srcRect);

        [ComMethodId(D2D1Resource.LastMethodId + 6u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate int CopyFromRenderTargetDelegate(IntPtr thisPtr, in D2D1Point2U destPoint, IntPtr renderTargetPtr,
            in D2D1RectU srcRect);

        [ComMethodId(D2D1Resource.LastMethodId + 7u), UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate int CopyFromMemoryDelegate(IntPtr thisPtr, in D2D1RectU destRect, IntPtr srcData, uint pitch);
    }
}