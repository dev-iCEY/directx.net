﻿namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Specifies how simple the output of a simplified geometry sink should be.
    /// </summary>
    public enum D2D1GeometrySimplificationOption : uint {
        CubicsAndLines = 0,
        Lines = 1
    }
}