﻿#region Includes

using System;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Describes whether a window is occluded.
    /// </summary>
    [Flags]
    public enum D2D1WindowState : uint {
        None = 0x0000000,
        Occluded = 0x0000001
    }

    public static class D2D1WindowStateExtensions {
        public static bool HasFlagFast(this D2D1WindowState value, D2D1WindowState flag) {
            return (value & flag) != 0;
        }
    }
}