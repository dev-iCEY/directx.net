﻿namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Differentiates which of the two possible arcs could match the given arc parameters.
    /// </summary>
    public enum D2D1ArcSize : uint {
        Small = 0,
        Large = 1
    }
}