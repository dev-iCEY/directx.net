﻿namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Defines the direction that an elliptical arc is drawn.
    /// </summary>
    public enum D2D1SweepDirection : uint {
        CounterClockwise = 0,
        Clockwise
    }
}