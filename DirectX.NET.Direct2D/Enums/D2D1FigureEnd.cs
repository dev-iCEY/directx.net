﻿namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Indicates whether the figure is open or closed on its end point.
    /// </summary>
    public enum D2D1FigureEnd : uint {
        Open = 0,
        Closed = 1
    }
}