﻿#region Includes

using System.Runtime.InteropServices;

using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the gradient origin offset and the size and position of the gradient ellipse for an
    ///     <see cref="ID2D1RadialGradientBrush" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1RadialGradientBrushProperties {
        public D2D1Point2F Center { get; set; }
        public D2D1Point2F GradientOriginOffset { get; set; }
        public float RadiusX { get; set; }
        public float RadiusY { get; set; }

        public D2D1RadialGradientBrushProperties(D2D1Point2F center, D2D1Point2F gradientOriginOffset, float radiusX,
            float radiusY) {
            Center = center;
            GradientOriginOffset = gradientOriginOffset;
            RadiusX = radiusX;
            RadiusY = radiusY;
        }
    }
}