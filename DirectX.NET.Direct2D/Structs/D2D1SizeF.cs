﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1SizeF {
        public float Width { get; set; }
        public float Height { get; set; }

        public D2D1SizeF(float width, float height) {
            Width = width;
            Height = height;
        }
    }
}