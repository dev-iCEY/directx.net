﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Vector3F {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public D2D1Vector3F(float x, float y, float z) {
            X = x;
            Y = y;
            Z = z;
        }
    }
}