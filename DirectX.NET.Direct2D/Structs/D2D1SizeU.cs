﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1SizeU {
        public uint Width { get; set; }
        public uint Height { get; set; }

        public D2D1SizeU(uint width, uint height) {
            Width = width;
            Height = height;
        }
    }
}