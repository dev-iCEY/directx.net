﻿#region Includes

using System.Runtime.InteropServices;

using DirectX.NET.DXGI;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1PixelFormat {
        public DXGIFormat Format { get; set; }
        public D2D1AlphaMode AlphaMode { get; set; }
    }
}