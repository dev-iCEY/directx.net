﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the control point and end point for a quadratic Bezier segment.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1QuadraticBezierSegment {
        public D2D1Point2F Point1;
        public D2D1Point2F Point2;
    }
}