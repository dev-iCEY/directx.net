﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1RectF {
        public float Left { get; set; }
        public float Top { get; set; }
        public float Right { get; set; }
        public float Bottom { get; set; }

        public D2D1RectF(float left, float top, float right, float bottom) {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }
}