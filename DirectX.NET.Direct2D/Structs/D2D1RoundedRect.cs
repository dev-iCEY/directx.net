﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the dimensions and corner radii of a rounded rectangle.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1RoundedRect {
        public D2D1RectF Rect;
        public float RadiusX;
        public float RadiusY;

        public D2D1RoundedRect(D2D1RectF rect, float radiusX, float radiusY) {
            Rect = rect;
            RadiusX = radiusX;
            RadiusY = radiusY;
        }
    }
}