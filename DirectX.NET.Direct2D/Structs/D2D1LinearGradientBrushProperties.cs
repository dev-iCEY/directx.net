﻿#region Includes

using System.Runtime.InteropServices;

using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the starting point and endpoint of the gradient axis for an <see cref="ID2D1LinearGradientBrush" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public ref struct D2D1LinearGradientBrushProperties {
        public D2D1Point2F StartPoint { get; set; }
        public D2D1Point2F EndPoint { get; set; }

        public D2D1LinearGradientBrushProperties(D2D1Point2F startPoint, D2D1Point2F endPoint) {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }
    }
}