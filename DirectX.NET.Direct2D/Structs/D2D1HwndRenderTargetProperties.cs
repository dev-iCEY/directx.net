﻿#region Includes

using System;
using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the HWND, pixel size, and presentation options for an <see cref="Interfaces.ID2D1HwndRenderTarget" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1HwndRenderTargetProperties {
        public IntPtr WindowHandle;
        public D2D1SizeU PixelSize;
        public D2D1PresentOptions PresentOptions;

        public D2D1HwndRenderTargetProperties(IntPtr windowHandle, D2D1SizeU pixelSize, D2D1PresentOptions presentOptions) {
            WindowHandle = windowHandle;
            PixelSize = pixelSize;
            PresentOptions = presentOptions;
        }
    }
}