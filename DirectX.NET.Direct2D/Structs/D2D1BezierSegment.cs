﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Describes a cubic bezier in a path.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1BezierSegment {
        public D2D1Point2F Point1;
        public D2D1Point2F Point2;
        public D2D1Point2F Point3;
    }
}