﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains the center point, x-radius, and y-radius of an ellipse.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Ellipse {
        public D2D1Point2F Point;
        public float RadiusX;
        public float RadiusY;

        public D2D1Ellipse(D2D1Point2F point, float radiusX, float radiusY) {
            Point = point;
            RadiusX = radiusX;
            RadiusY = radiusY;
        }
    }
}