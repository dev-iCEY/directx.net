﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Describes the extend modes and the interpolation mode of an <see cref="IBitmapBrush" />.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1BitmapBrushProperties {
        public D2D1ExtendMode ExtendModeX { get; set; }
        public D2D1ExtendMode ExtendModeY { get; set; }
        public D2D1BitmapInterpolationMode InterpolationMode { get; set; }
    }
}