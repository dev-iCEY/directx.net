﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Properties, aside from the width, that allow geometric penning to be specified.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1StrokeStyleProperties {
        public D2D1CapStyle StartCap;
        public D2D1CapStyle EndCap;
        public D2D1CapStyle DashCap;
        public D2D1LineJoin LineJoin;
        public float MiterLimit;
        public D2D1DashStyle DashStyle;
        public float DashOffset;

        public D2D1StrokeStyleProperties(D2D1CapStyle startCap, D2D1CapStyle endCap, D2D1CapStyle dashCap, D2D1LineJoin lineJoin,
            float miterLimit, D2D1DashStyle dashStyle, float dashOffset) {
            StartCap = startCap;
            EndCap = endCap;
            DashCap = dashCap;
            LineJoin = lineJoin;
            MiterLimit = miterLimit;
            DashStyle = dashStyle;
            DashOffset = dashOffset;
        }
    }
}