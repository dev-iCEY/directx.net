﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Describes an arc that is defined as part of a path.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1ArcSegment {
        public D2D1Point2F Point;
        public D2D1SizeF Size;
        public float AngleRotation;
        public D2D1SweepDirection SweepDirection;
        public D2D1ArcSize ArcSize;
    }
}