﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Tag {
        public D2D1Tag(ulong value) {
            Value = value;
        }

        public ulong Value;

        public static implicit operator ulong(D2D1Tag tag) {
            return tag.Value;
        }

        public static implicit operator D2D1Tag(ulong value) {
            return new D2D1Tag(value);
        }
    }
}