﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Describes a triangle.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Triangle {
        public D2D1Point2F Point1;
        public D2D1Point2F Point2;
        public D2D1Point2F Point3;

        public D2D1Triangle(D2D1Point2F point1, D2D1Point2F point2, D2D1Point2F point3) {
            Point1 = point1;
            Point2 = point2;
            Point3 = point3;
        }
    }
}