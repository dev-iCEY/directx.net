﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Allows the drawing state to be atomically created. This also specifies the drawing state that is saved into an
    ///     IDrawingStateBlock object.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1DrawingStateDescription {
        public D2D1AntialiasMode AntialiasMode;
        public D2D1TextAntialiasMode TextAntialiasMode;
        public D2D1Tag Tag1;
        public D2D1Tag Tag2;
        public Matrix3X2 Transform;
    }
}