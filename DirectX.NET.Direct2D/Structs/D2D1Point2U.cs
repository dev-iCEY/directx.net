﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Point2U {
        public uint X { get; set; }
        public uint Y { get; set; }

        public D2D1Point2U(uint x, uint y) {
            X = x;
            Y = y;
        }
    }
}