﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1RectU {
        public uint Left { get; set; }
        public uint Top { get; set; }
        public uint Right { get; set; }
        public uint Bottom { get; set; }

        public D2D1RectU(uint left, uint top, uint right, uint bottom) {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }
}