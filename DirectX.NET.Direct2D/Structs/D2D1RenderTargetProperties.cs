﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    /// <summary>
    ///     Contains rendering options (hardware or software), pixel format, DPI information, remoting options, and Direct3D
    ///     support requirements for a render target.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1RenderTargetProperties {
        public D2D1RenderTargetType Type;
        public D2D1PixelFormat PixelFormat;
        public float DpiX;
        public float DpiY;
        public D2D1RenderTargetUsage Usage;
        public D2D1FeatureLevel MinLevel;
    }
}