﻿#region Includes

using System.Runtime.InteropServices;

#endregion

namespace DirectX.NET.Direct2D {
    [StructLayout(LayoutKind.Sequential)]
    public struct D2D1Point2F {
        public float X { get; set; }
        public float Y { get; set; }

        public D2D1Point2F(float x, float y) {
            X = x;
            Y = y;
        }
    }
}