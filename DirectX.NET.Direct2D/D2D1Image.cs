﻿#region Includes

using System;

using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    public class D2D1Image : D2D1Resource, ID2D1Image {
        public D2D1Image(IntPtr objectPtr) : base(objectPtr) {
        }
    }
}