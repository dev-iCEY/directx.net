﻿#region Includes

using System;

using DirectX.NET.Direct2D.Interfaces;

#endregion

namespace DirectX.NET.Direct2D {
    public class D2D1Factory : Unknown, ID2D1Factory {
        public D2D1Factory(IntPtr objectPtr) : base(objectPtr) {
        }

        public int ReloadSystemMetrics() {
            throw new NotImplementedException();
        }

        public void GetDesktopDpi(out float dpiX, out float dpiY) {
            throw new NotImplementedException();
        }
    }
}